function timSoNguyenDuongNhoNhat() {
    var number = 500;
    var tong = 0;
    var count = 0;
    for (var i = 1; i <= number; i++) {
        tong = tong + i;
        count++;
        if (tong > 10000) {
            var contentHTMLBai1 = `<h3>Số cần tìm là: ${count}</h3>`;
            document.querySelector("#resultBai1").innerHTML = contentHTMLBai1;
            break;
        }
    }
}
function tinhTong() {
    var soX = document.querySelector("#x").value * 1;
    document.querySelector("#x").value = "";
    var soN = document.querySelector("#n").value * 1;
    document.querySelector("#n").value = "";
    var term = 1;
    var sum = 0;
    for (var i = 0; i <= soN; i++) {
        term = term * soX;
        sum = sum + term;
    }
    var contentHTMLBai2 = `<h3>Tổng từ ${soX} đến ${soX}^${soN} là: ${sum}</h3>`;
    document.querySelector("#resultBai2").innerHTML = contentHTMLBai2;
}
function tinhGiaiThua() {
    var n = document.querySelector("#soN").value * 1;
    document.querySelector("#soN").value = "";
    var giaiThua = 1;

    for (var i = 1; i <= n; i++) {
        giaiThua = giaiThua * i;
    }
    var contentHTMLBai3 = `<h3>Giai thừa của ${n} là: ${giaiThua}</h3>`;
    document.querySelector("#resultBai3").innerHTML = contentHTMLBai3;
}
function click10div() {
    var div = 10;
    var resultDiv = document.querySelector("#resultBai4");

    for (var i = 1; i <= div; i++) {
        var divHTML = "";
        if (i % 2 == 0) {
            divHTML = `<div style="background-color: red;">Div chẵn ${i}</div>`;
        } else {
            divHTML = `<div Style="background-color: green;">Div lẻ ${i}</div>`;
        }
        resultDiv.innerHTML += divHTML;
    }
}
